//const Question = require("../model/question_model.js");
//const Question = require("./question_model.js");

import Question from "./question_model.js";
import View from "./question_view.js";
import View2 from "./question_view2.js";
import * as fs from 'fs';

import * as path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//const fs = require('fs');


class QuestionController {

  	constructor() {

    }  
	// Retrieve all Questions from the database (with condition).
	findAll (req, res) {

	  const title = req.query.title;
	  let questionModel = new Question();
	  let view = new View();
	   
	  questionModel.getAll(title).then(function(result){
		//console.log(result);
		view.setDiv(result);
		let html = view.render();
		res.send(html);
	  }).catch(function(err){
	  	console.log("Promise rejection error: "+err);
	  })

	}
	
	findAllWithStr (req, res) {

	  const title = req.query.title;
	  let questionModel = new Question();
	  let view = new View2();
	  
	  console.log(__dirname);
	  var strHtmlData = fs.readFileSync(path.join(__dirname, 'question_tmp.html'), 'utf8');
	  console.log(strHtmlData);
	  view.setTempate(strHtmlData);
	   
	  questionModel.getAll(title).then(function(result){
		//console.log(result);
		view.setQuestions(result);
		let html = view.render();
		res.send(html);
	  }).catch(function(err){
	  	console.log("Promise rejection error: "+err);
	  })

	}
	
	
		//Submit a question and insert the question into the table
	submitQuestion (req, res) {

	 let questionModel = new Question();
	 let questionData = {};
	 questionData["title"] = req.query.title;
	 questionData["content"] = req.query.content;
	 questionData["is_answered"] = 0;
	  //let view = new View2();
	  //console.log(__dirname);
	  //let tmplate = fs.readFileSync(path.join(__dirname, 'question_tmp.html'),
      //      {encoding:'utf8', flag:'r'});
	  //console.log(tmplate);
	  
	  //view.setTemplate(tmplate);
	   
	  questionModel.create("question", questionData).then(function(result){
		//console.log(result);
		//view.setDiv(result);
		//let html = view.render();
		res.send("Success");
	  }).catch(function(err){
	  	console.log("Promise rejection error: "+err);
	  })
	}
	
	
	//Update a question and insert the question into the table
	updateQuestionById (req, res) {

	 let questionModel = new Question();
	 let questionData = {};
	 let whereData = {};
	 if (req.query.content != "") {
	 	 questionData = {"title": req.query.title, "content": req.query.content};
	 } else {
		 questionData = {"title": req.query.title}
	 }
	 whereData["id"] = req.query.id;

	 questionModel.update("question", whereData ,questionData).then(function(result){
		res.send("Success");
	 }).catch(function(err){
	  	console.log("Promise rejection error: "+err);
	 })

	}


}


export default QuestionController;

