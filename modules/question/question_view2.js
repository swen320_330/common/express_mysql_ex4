class View2 {

  	constructor() {
	  this.template = "";
	  this.html = "";
    }  


	setTempate(data) {
	  this.template = data;
	}
	
	setQuestions(data) {
	 let divHtml = "";
		for (var key in data) {
   			if (data.hasOwnProperty(key)) {
				divHtml = divHtml + "<div id='q_" +  data[key].id + "' class='q_title' >" + data[key].title + "</div>";
   			}
		}

	  this.html = this.template.replace("@_question_@", divHtml);
	  console.log(this.html);
	}
	
	render() {
		
		return this.html; 
	}
}

export default View2;