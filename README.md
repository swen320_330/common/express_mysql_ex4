# express_mysql_ex4

This is an improved version 4 of using express and mysql. This example follows MVC pattern.

## Getting started

- Setup the docker container
- Run the docker container
- Create a testdb in MySQL server
- Run db_v1.sql inside the db_backup folder
- NOTE: for the first execution, you need to run "npm install" to install node.js packages.
- type "npm start" and run the express application

## Ensure successful execution
- Use your browser enter "localhost:3000" to see if application runs

